# Projekt AK2

## Jak to odpalić?

### Linux

**Po zainstalowaniu g++** odpalić w termnialu komendy:

    > make
    > bash generate.sh
    > ./start

### Windows

1. Ściągnąć i zainstalować [MinGW-w64](http://mingw-w64.org/doku.php), polecam wersję [Mingw-builds](http://sourceforge.net/projects/mingw-w64/files/Toolchains%20targetting%20Win32/Personal%20Builds/mingw-builds/installer/mingw-w64-install.exe/download) (w trakcie instalacji zaznaczyć wersję  **x86_64**)
2. Dodać folder bin z instalacji MinGW do PATH

##### W folderze projektu odpalić komendy w zależności od terminala:

###### Powershell albo windows shell:

    > mingw32-make
    > .\generate.sh
    > .\start.exe

###### Bash:

    > mingw32-make
    > bash generate.sh
    > ./start.exe
