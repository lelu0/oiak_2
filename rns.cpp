#include <fstream>
#include <cstdio>
#include <iostream>
#include <cstdlib>
#include <bitset>

using namespace std;

//
// BUFFOR 
//

const int bufferSize = 16;
char buffer[bufferSize];

char buffer2[bufferSize];

//
// NUMBER 128 BITS
//

typedef unsigned long long int b64;

struct b128
{
public:
    b64 low, high;
    
    b128(b64 insertLowPart, b64 insertHighPart)
    {
        low = insertLowPart;
        high = insertHighPart;
    }
};

//
// VALUE REPRESENTATION RNS (m)
//
// 2^30 -1 = 1073741823
// 2^30 +1 = 1073741825
// 2^31 = 2147483648
// 2^37 -1 = 137438953471
//
//
// m^
//
// (2^30+1)*(2^31)*(2^37-1) mod (2^30-1) = 508
// (2^30-1)*(2^31)*(2^37-1) mod (2^30+1) = 1073741309
// (2^30+1)*(2^30-1)*(2^37-1) mod (2^31) = 1
// (2^30+1)*(2^30-1)*(2^31) mod (2^37-1) = 135291600895
//
//
// ( k * m^) mod(m) = 1 => k=? 
//  
//  2^30-1 => 357209386
//  2^30+1 => 859409639
//  2^31   => 1
//  2^37-1 => 119150671739
//



struct RNSRepresentation
{
public:
    b64 module30minus;
    b64 module30plus;
    b64 module31;
    b64 module37minus;
        
    RNSRepresentation(b64 a, b64 b, b64 c, b64 d)
    {
        module30minus = a;
        module30plus = b;
        module31 = c;
        module37minus = d;
    }
    
    RNSRepresentation()
    {}
};

//
// DECLARATIONS
//

void pressEnterToContinue();
bool readBitsFromBinaryFile(char*);
void printBuffer();
void printBuffer2();
void printBuffer4();
RNSRepresentation createNumber(char*);
b128 calculateValueOfBuffer();
b64 computeModule31();
b64 computeModule30minus();
b64 computeModule30plus();
b64 computeModule37minus();
b64 computeAddModule(b64, b64, b64(*)());
RNSRepresentation rnsAdd(RNSRepresentation, RNSRepresentation);
b64 computeMulModule(b64, b64, b64(*)());
RNSRepresentation rnsMul(RNSRepresentation, RNSRepresentation);
void reverseConversion(RNSRepresentation);


//
// EXECUTABLE PART
//

int main()
{    
    //cout << "----------------------------\n"\
         << "Projekt AK2 - RNS\n"\
         << "Wt/NP 7:30\n"\
         << "Adrian Konopka\n"\
         << "----------------------------\n";
    //mixRadix/porownanie czasow  // konw rns mixRadix + dzialaia + pozycyjna + porowanie czasu, pamieci


    RNSRepresentation rns1 = createNumber((char*)"file2.bin");
    //cout << " comp " << computeModule30plus();
    //cout <<" in rns1 "<< rns1.module30plus << "\n";
    printBuffer2();
    
    //pressEnterToContinue();
       
    RNSRepresentation rns2 = createNumber((char*)"file2.bin");
    //cout << " comp " << computeModule30plus();
    //cout <<" in rns2 "<< rns2.module30plus << "\n";
    printBuffer2();
    
    //cout << "\nDodawanie:\n";
    
    //pressEnterToContinue();
    //rns2 = rnsAdd(rns1, rns2);
    
    //reverseConversion(rns2);
    
    //printBuffer();
    //printBuffer4();
    //cout << "\nDodawanie:\n";
    //pressEnterToContinue();
    
    rns1 = rnsAdd(rns1, rns2);
    //cout << "\nMnożenie:\n";
    //pressEnterToContinue();    
    
    //rns1 = rnsAdd(rns1, rns2);    
    
    //printBuffer();
    pressEnterToContinue();
    
    reverseConversion(rns2);
    
    return 0;
}

//
// IMPLEMENTATIONS
//

void pressEnterToContinue()
{
    std::cout << "Press enter to continue...";
    std::cin.get();
}

bool readBitsFromBinaryFile(char* nameOfFile)
{
    ifstream file(nameOfFile, ios::binary);
    file.read(buffer, bufferSize);
    if (!file) {
        return 0;
    }
    if (!file.read(buffer, bufferSize)) {
        return 0;
    }
    return 1;
}

void printBuffer()
{
    cout << "\n";
    
    cout << "\n";
    cout << "higher 64 bytes: " << calculateValueOfBuffer().high << "\n";
    for(int i = 15; i>=8; i--)
    {
        cout << bitset<8>(buffer[i]);        
    }
    cout << "\n";
    cout << "lower 64 bytes: " << calculateValueOfBuffer().low << "\n";
    for(int i = 7; i>=0; i--)
    {
        cout << bitset<8>(buffer[i]);
    }
    cout << "\n";    
}

void printBuffer2()
{
    cout << "\n";
    
    cout << "\n";
    for(int i = 15; i>=8; i--)
    {
        cout << bitset<8>(buffer[i]);        
    }
    for(int i = 7; i>=0; i--)
    {
        cout << bitset<8>(buffer[i]);
    }
    cout << "\n";    
}

void printBuffer4()
{
    cout << "\n";
    
    cout << "\n";
    for(int i = 15; i>=8; i--)
    {
        //cout << bitset<8>(buffer2[i]);        
    }
    for(int i = 7; i>=0; i--)
    {
        cout << bitset<8>(buffer2[i]);
    }
    for(int i = 15; i>=8; i--)
    {
        cout << bitset<8>(buffer[i]);        
    }
    for(int i = 7; i>=0; i--)
    {
        cout << bitset<8>(buffer[i]);
    }
    cout << "\n";    
}

RNSRepresentation createNumber(char* nameOfFile)
{
    readBitsFromBinaryFile(nameOfFile);
    
    RNSRepresentation outputNumber = RNSRepresentation();
    outputNumber.module30minus = computeModule30minus();
    outputNumber.module31 = computeModule31();
    outputNumber.module37minus = computeModule37minus();
    outputNumber.module30plus = computeModule30plus();
    
    cout << "\ndebug\n" << outputNumber.module30minus << " " << outputNumber.module30plus << " " << outputNumber.module31 << " " << outputNumber.module37minus << "\n";
    return outputNumber;    
}

b128 calculateValueOfBuffer()
{
    b64 low = 0, high = 0;    
    __asm__ (
        "movq buffer(, %%rcx, 8), %%rbx\n"
        "incq %%rcx\n"
        "movq buffer(, %%rcx, 8), %%rax\n"
        : "=a" (high), "=b" (low)
        : "c" (0)
    );
    
    return b128(low, high);    
}

b64 computeModule31()
{
    b64 outputNumber = 0;
    __asm__ (
        "movq buffer(, %%rcx, 8), %%rax\n"
        "andq $0x000000007FFFFFFF, %%rax\n"
        : "=a" (outputNumber)
        : "c" (0)
    );
    cout << "\n module31: " << outputNumber;
    return outputNumber;
}

b64 computeModule30minus()
{    
    b64 outputNumber = 0;
    __asm__(
        // put buffer to registers
        "movq buffer(, %%rcx, 8), %%r15\n" // low
        "incq %%rcx\n"
        "movq buffer(, %%rcx, 8), %%r14\n" // high
        "movb %3, %%cl\n" // shift
        
        // mask
        "movq $1, %%r8\n"
        "shlq %%cl, %%r8\n"
        "decq %%r8\n"
        
        "loop30minus:\n"
        // copy value for operations
        "movq %%r15, %%rbx\n"        
        "movq %%r14, %%rax\n"
        
        // low & maska, add
        "andq %%r8, %%rbx\n"        
        "addq %%rbx, %%rdx\n" 
        
        // bit shift
        "movq %%r15, %%rbx\n"
        "shrq %%cl, %%rbx\n"
        "andq %%r8, %%rax\n"
        "rorq %%cl, %%rax\n"
        "orq %%rax, %%rbx\n"
        "movq %%r14, %%rax\n"   
        "shrq %%cl, %%rax\n"
        
        // save bit shift
        "movq %%rbx, %%r15\n"
        "movq %%rax, %%r14\n"
        
        // if rax > 0 jump to label loop30minus: 
        "cmp $0, %%rbx\n"
        "jnz loop30minus\n"
        "cmp $0, %%rax\n"
        "jnz loop30minus\n"
        
        "movq %%r8, %%rcx\n"
      
        : "=d" (outputNumber)
        : "d" (outputNumber), "c" (0), "r" ((char)30)
    );
    cout << "\n module30minus: " << outputNumber;
    return outputNumber;
}

b64 computeModule30plus()
{   
    //cout << "\nstart function\n";
    b64 outputNumber = 0;
    __asm__(
        // put buffer to registers
        "movq buffer(, %%rcx, 8), %%r15\n" // low
        "incq %%rcx\n"
        "movq buffer(, %%rcx, 8), %%r14\n" // high
        "movb %3, %%cl\n" // shift
        "movb %4, %%r10b\n" // count
        
        // mask
        "movq $1, %%r8\n"
        "shlq %%cl, %%r8\n"
        "decq %%r8\n"
        
        "loop30plus:\n"
        // copy value for operations
        "movq %%r15, %%rbx\n"        
        "movq %%r14, %%rax\n"
        
        // low & maska, add
        "andq %%r8, %%rbx\n" 
        
        // if %2 == 0
        "testb $2, %%r10b\n"
        "jz notmod2\n"
        "mod2:\n"
        "decb %%r10b\n"
        "addq %%rbx, %%rdx\n"
        "jmp aftermod2\n"
        "notmod2:\n"
        "incb %%r10b\n"
        "sbbq %%rbx, %%rdx\n"
        "aftermod2:\n"
        
        // verify overflow
        "jnc aftercarry\n"
        "addq %%r8 ,%%rdx\n"
        "aftercarry:\n"
        
        // bit shift
        "movq %%r15, %%rbx\n"
        "shrq %%cl, %%rbx\n"
        "andq %%r8, %%rax\n"
        "rorq %%cl, %%rax\n"
        "orq %%rax, %%rbx\n"
        "movq %%r14, %%rax\n"   
        "shrq %%cl, %%rax\n"
        
        // save bit shift
        "movq %%rbx, %%r15\n"
        "movq %%rax, %%r14\n"
        
        
        
        // if rax > 0 jump to label loop30plus: 
        "cmp $0, %%rbx\n"
        "jnz loop30plus\n"
        "cmp $0, %%rax\n"
        "jnz loop30plus\n"
        
        "movq %%r8, %%rcx\n"
      
        : "=d" (outputNumber)
        : "d" (outputNumber), "c" (0), "r" ((char)30), "r" ((char)2)
    );
    cout << "\n module30plus: " << outputNumber;
    return outputNumber;
}

b64 computeModule37minus()
{    
    b64 outputNumber = 0;
    __asm__(
        // put buffer to registers
        "movq buffer(, %%rcx, 8), %%r15\n" // low
        "incq %%rcx\n"
        "movq buffer(, %%rcx, 8), %%r14\n" // high
        "movw %4, %%cx\n"
        "shlw $8, %%cx\n"
        "movb %3, %%cl\n" // shift
        //"movb %4, %%ch\n"
        
        
        // mask
        "movq $1, %%r8\n"
        "shlq %%cl, %%r8\n"
        "rorw $8, %%cx\n"
        "shlq %%cl, %%r8\n"
        "rorw $8, %%cx\n"
        "decq %%r8\n"
        
        // copy value for operations
        "movq %%r15, %%rbx\n"
        "loop37minus:\n"
        "movq %%r14, %%rax\n"
        
        // low & maska, add
        "andq %%r8, %%rbx\n"        
        "addq %%rbx, %%rdx\n" 
        
        // bit shift
        "movq %%r15, %%rbx\n"
        "shrq %%cl, %%rbx\n"
        "rorw $8, %%cx\n"
        "shrq %%cl, %%rbx\n"
        "rorw $8, %%cx\n"
        "andq %%r8, %%rax\n"
        "rorq %%cl, %%rax\n"
        "rorw $8, %%cx\n"
        "rorq %%cl, %%rax\n"
        "rorw $8, %%cx\n"
        "orq %%rax, %%rbx\n"
        "movq %%r14, %%rax\n"   
        "shrq %%cl, %%rax\n"
        "rorw $8, %%cx\n"
        "shrq %%cl, %%rax\n"
        "rorw $8, %%cx\n"
        
        // save bit shift
        "movq %%rbx, %%r15\n"
        "movq %%rax, %%r14\n"
        
        // if rax > 0 jump to label loop37minus: 
        "cmp $0, %%rbx\n"
        "jnz loop37minus\n"
        "cmp $0, %%rax\n"
        "jnz loop37minus\n"
      
        : "=d" (outputNumber)
        : "d" (outputNumber), "c" (0), "r" ((char)30), "r" ((short)7)
    );
    cout << "\n module37minus: " << outputNumber;
    return outputNumber;
}

b64 computeAddModule(b64 inFirst, b64 inSec, b64(*computeModuleFunction)())
{
     b64 outputNumber = 0;
    __asm__ (
        "addq %%rbx, %%rax\n"
        "movq %%rax, buffer(, %%rcx, 8)\n" // low
        "incq %%rcx\n"
        "movq $0, buffer(, %%rcx, 8)\n" // high
        :: "a" (inFirst), "b" (inSec), "c" (0)
    );    
    
    //printBuffer();
    
    outputNumber = computeModuleFunction();
    
    return outputNumber;    
}

RNSRepresentation rnsAdd(RNSRepresentation first, RNSRepresentation second)
{
    RNSRepresentation result;
    result.module30minus = computeAddModule(first.module30minus, second.module30minus, computeModule30minus);
    result.module30plus = computeAddModule(first.module30plus, second.module30plus, computeModule30plus); 
    //cout << " rns1 + rns2 = " << first.module30plus <<  " " << second.module30plus << " " << result.module30plus << "\n";
    //result.module30plus = first.module30plus + second.module30plus;
    result.module31 = computeAddModule(first.module31, second.module31, computeModule31);
    //cout << " rns1 + rns2 = " << first.module31 <<  " " << second.module31 << " " << result.module31;
    result.module37minus = computeAddModule(first.module37minus, second.module37minus, computeModule37minus);
    
    return result;
}

b64 computeMulModule(b64 inFirst, b64 inSec, b64(*computeModuleFunction)())
{
    b64 outputNumber = 0;
    __asm__ (
        "mulq %%rbx\n"
        "movq %%rax, buffer(, %%rcx, 8)\n" // low
        "incq %%rcx\n"
        "movq %%rdx, buffer(, %%rcx, 8)\n" // high
        :: "a" (inFirst), "b" (inSec), "c" (0)
    );    
    
    //printBuffer();
    
    outputNumber = computeModuleFunction();
    
    return outputNumber;
}

RNSRepresentation rnsMul(RNSRepresentation first, RNSRepresentation second)
{
    RNSRepresentation result(0,0,0,0);
    result.module30minus = computeMulModule(first.module30minus, second.module30minus, computeModule30minus);
    result.module30plus = computeMulModule(first.module30plus, second.module30plus, computeModule30plus); 
    result.module31 = computeMulModule(first.module31, second.module31, computeModule31);
    result.module37minus = computeMulModule(first.module37minus, second.module37minus, computeModule37minus);
    
    return result;
}


void reverseConversion(RNSRepresentation rns)
{
   cout<< " Rev conversion!!!!!!!\n";
    b64 modul_1_2 = 0,\
        modul_2_2 = 0,\
        modul30minus_1_2 = 0,\
        modul30minus_2_2 = 0,\
        modul30plus_1_2 = 0,\
        modul30plus_2_2 = 0,\
        modul31_1_2 = 0,\
        modul31_2_2 = 0,\
        modul37minus_1_2 = 0,\
        modul37minus_2_2 = 0,\
        k30minus = 357209386,\
        k30plus = 859409639,\
        k31 = 1,\
        k37minus = 119150671739;
        /*mr30minus = 508,\
        mr30plus = 1073741309,\
        mr31 = 1,\
        mr37minus = 135291600895,\*/
        
    __asm__(
        "mulq %%rbx\n"
        "movq %%rax, buffer(, %%rcx, 8)\n" // low
        "incq %%rcx\n"
        "movq %%rdx, buffer(, %%rcx, 8)\n" // high
        :: "a" (k30minus), "b" (rns.module30minus), "c" ((b64)0)        
    );
        rns.module30minus = computeModule30minus();
        
       //cout << " 30m " << rns.module30minus;
        
    __asm__(
        "mulq %%rbx\n"
        "movq %%rax, buffer(, %%rcx, 8)\n" // low
        "incq %%rcx\n"
        "movq %%rdx, buffer(, %%rcx, 8)\n" // high
        :: "a" (k30plus), "b" (rns.module30plus), "c" ((b64)0)        
    );
        rns.module30plus = computeModule30plus();
        
        //cout << " 30p " = rns.module30plus;
        
        //cout << rns.module31 << "\n\n";
    __asm__(
        "mulq %%rbx\n"
        "movq %%rax, buffer(, %%rcx, 8)\n" // low
        "incq %%rcx\n"
        "movq %%rdx, buffer(, %%rcx, 8)\n" // high
        :: "a" (k31), "b" (rns.module31), "c" ((b64)0)        
    );
        rns.module31 = computeModule31();
        
        //cout << " 31 " << computeModule31();
        
    __asm__(
        "mulq %%rbx\n"
        "movq %%rax, buffer(, %%rcx, 8)\n" // low
        "incq %%rcx\n"
        "movq %%rdx, buffer(, %%rcx, 8)\n" // high
        :: "a" (k37minus), "b" (rns.module37minus), "c" ((b64)0)        
    );
        rns.module37minus = computeModule37minus();
        
        cout << "\n 30m " << rns.module30minus << "\n";
        cout << " 30p " << rns.module30plus << "\n";
        cout << " 31 " << rns.module31 << "\n";
        cout << " 37m " << rns.module37minus << "\n";
        
        //printBuffer();
        
        // do tego momentu jest na pewno poprawne, szukac bledu dalej
        
    __asm__ (
        // liczenie modulu calkowitego      L: 4, H: 5
        "movq $1, %%rax\n"
        "shlq $30, %%rax\n"
        "shlq $7, %%rax\n"
        "decq %%rax\n"
        "shlq $16, %%rax\n"
        //"rorq $7, %%rax\n"
        "addq $11, %%rax\n"
        "shlq $11, %%rax\n"
        "movq $0, %4\n"
        "movq %%rax, %5\n"
        
        // liczenie modulu bez 2^30 - 1     L: 6, H: 7
        "movq $1, %%rax\n"
        "shlq $30, %%rax\n"
        "decq %%rax\n"
        
        "movq %%rax, %%rbx\n"
        "movq %5, %%rax\n"
        "divq %%rbx\n"
                
        "movq %%rdx, %6\n"
        "movq %%rax, %7\n"
        
        // liczenie modulu bez 2^30 + 1     L: 8, H: 9
        "movq $1, %%rax\n"
        "shlq $30, %%rax\n"
        "incq %%rax\n"
        "movq $0, %%rdx\n"
        
        "movq %%rax, %%rbx\n"
        "movq %5, %%rax\n"
        "divq %%rbx\n"
                
        "movq %%rdx, %8\n"
        "movq %%rax, %9\n"
                   
        // liczenie modulu bez 2^31         L: 10, H: 11
        "movq $1, %%rax\n" 
        "shlq $31, %%rax\n"
        "movq $0, %%rdx\n"
        
        "movq %%rax, %%rbx\n"
        "movq %5, %%rax\n"
        "divq %%rbx\n"
                
        "movq %%rdx, %10\n"
        "movq %%rax, %11\n"
        
        // liczenie modulu bez 2^37 - 1     L: 12, H: 13
        "movq $1, %%rax\n" 
        "shlq $30, %%rax\n"
        "shlq $7, %%rax\n"
        "decq %%rax\n"
        "movq $0, %%rdx\n"
        
        "movq %%rax, %%rbx\n"
        "movq %5, %%rax\n"
        "divq %%rbx\n"
                
        "movq %%rdx, %12\n"
        "movq %%rax, %13\n"
        
        // r9 niskie-niskie, r10 niskie-wysokiego, r11 wysokie-wysokiego, r12 w-w-w
        
        "movq $0, %%r8\n"
        "movq $0, %%r9\n"
        "movq $0, %%r10\n"
        "movq $0, %%r11\n"
        "movq $0, %%r12\n"
        
        "movq $0, %%r14\n"
        "movq $0, %%r15\n"
        
        "movq $0, %%rax\n"
        "movq $0, %%rbx\n"
        "movq $0, %%rdx\n"
        
        
        // z(2^30-1) = 
        
        "movq %0, %%rax\n" // modul
        "movq %6, %%rbx\n" // niskie
        "mulq %%rbx\n"
        
        "addq %%rax, %%r10\n" // out niskie
        "adcq %%rdx, %%r11\n" // out wysokie
        
        "movq %0, %%rax\n" // modul
        "movq %7, %%rbx\n" //wysokie
        "mulq %%rbx\n"
        
        "addq %%rax, %%r11\n" // out wysokie
        "adcq %%rdx, %%r12\n" // out wysokie(poza zakresem)
        
        "movq $0, %%rax\n"
        "movq $0, %%rbx\n"
        
        // z(2^30+1) = 
        "movq %1, %%rax\n" // modul
        "movq %8, %%rbx\n" // niskie
        "mulq %%rbx\n"
        
        "addq %%rax, %%r10\n" // out niskie
        "adcq %%rdx, %%r11\n" // out wysokie
        
        "movq %1, %%rax\n" // modul
        "movq %9, %%rbx\n" //wysokie
        "mulq %%rbx\n"
        
        "addq %%rax, %%r11\n" // out wysokie
        "adcq %%rdx, %%r12\n" // out wysokie(poza zakresem)
        
        // dodawanie z(2^30+1)
        
        "movq $0, %%rax\n"
        "movq $0, %%rbx\n"
        
         // z(2^31) = 
        "movq %2, %%rax\n" // modul
        "movq %10, %%rbx\n" // niskie
        "mulq %%rbx\n"
        
        "addq %%rax, %%r10\n" // out niskie
        "adcq %%rdx, %%r11\n" // out wysokie
        
        "movq %2, %%rax\n" // modul
        "movq %11, %%rbx\n" //wysokie
        "mulq %%rbx\n"
        
        "addq %%rax, %%r11\n" // out wysokie
        "adcq %%rdx, %%r12\n" // out wysokie(poza zakresem)
        
        // dodawanie z(2^31)
               
        "movq $0, %%rax\n"
        "movq $0, %%rbx\n"
        
        // z(2^37-1) = 
        
        "movq %3, %%rax\n" // modul
        "movq %12, %%rbx\n" // niskie
        "mulq %%rbx\n"
        
        "addq %%rax, %%r10\n" // out niskie
        "adcq %%rdx, %%r11\n" // out wysokie
        
        "movq %3, %%rax\n" // modul
        "movq %13, %%rbx\n" //wysokie
        "mulq %%rbx\n"
        
        "addq %%rax, %%r11\n" // out wysokie
        "adcq %%rdx, %%r12\n" // out wysokie(poza zakresem)
        
        
    
        /*
        "movq %%r10, buffer(, %%rcx, 8)\n" // low
        "incq %%rcx\n"
        "movq %%r11, buffer(, %%rcx, 8)\n" // high
        */
        
        
        "movq %6, %%rax\n"
        "movq %%rax, buffer(, %%rcx, 8)\n" // low
        "incq %%rcx\n"
        "movq %7, %%rax\n"
        "movq %%rax, buffer(, %%rcx, 8)\n" // high
        
        "movq $0, %%rcx\n"
        "movq $0, buffer2(, %%rcx, 8)\n" // low
        "incq %%rcx\n"
        "movq %%r13, buffer2(, %%rcx, 8)\n" // high
        
        :: "r" (rns.module30minus),\
        "r" (rns.module30plus),\
        "r" (rns.module31),\
        "r" (rns.module37minus),\
        "m" (modul_1_2),\
        "m" (modul_2_2),\
        "m" (modul30minus_1_2),\
        "m" (modul30minus_2_2),\
        "m" (modul30plus_1_2),\
        "m" (modul30plus_2_2),\
        "m" (modul31_1_2),\
        "m" (modul31_2_2),\
        "m" (modul37minus_1_2),\
        "m" (modul37minus_2_2),\
        "c" (0)
        
    ); 
    printBuffer4();
}


